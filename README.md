### Tree Editor
My solution for task here.

#### [Demo](https://haritonasty.gitlab.io/tree-editor/)

#### Installation
```
git clone git@gitlab.com:haritonasty/tree-editor.git
cd tree-editor
npm i
npm start
```

#### Description
##### I used:
- React as a state manager (task isn't so big, I thought, that Redux/MobX are redundant)
- [immutability-helper](https://github.com/kolodny/immutability-helper) for copy tree
- Local Storage for saving tree
- [styled-components](https://www.styled-components.com/) for easy way to generate color for nodes

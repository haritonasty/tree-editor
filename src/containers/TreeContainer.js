import React from "react";
import styled from 'styled-components';
import update from 'immutability-helper';

import Node from "../components/Node";

const TreeEditor = styled.div`
  margin: 30px;
  padding: 0;
`;

const Heading = styled.h1`
  color: darkgrey;
  font-size: 14px;
  margin: 0 0 15px 0;
`;

const Link = styled.a`
  color: lightskyblue;
  margin: 0 5px;
`;


class TreeContainer extends React.Component {
  state = {};

  componentDidMount() {
    const tree = JSON.parse(localStorage.getItem('tree'));
    if (tree !== null) {
      this.setState({tree});
    } else {
      const defaultValue = {name: 'root', id: Date.now(), nodeChildren: []};
      this.setState({tree: defaultValue}, this.saveToLocalStorage);
    }
  }

  render() {
    return (
      <TreeEditor>
        <Heading>Tree Editor by
          <Link href='mailto:a.kharitonovaspb@gmail.com' title='a.kharitonovaspb@gmail.com'>
            Kharitonova Anastasya
          </Link>
          (@haritonasty)
        </Heading>
        {this.state.tree && (<Node
          id={this.state.tree.id}
          parentId={null}
          name={this.state.tree.name}
          nodeChildren={this.state.tree.nodeChildren}
          deleteNode={this.deleteNode}
          createNode={this.createNode}
          renameNode={this.renameNode}
        />)}
      </TreeEditor>
    )
  }

  deleteNode = (id, parentId) => {
    if (parentId === null) {
      this.setState({
        tree: {
          name: this.state.tree.name,
          id: this.state.tree.id,
          nodeChildren: []
        }
      }, this.saveToLocalStorage);
    } else {
      const tree = update(this.state.tree, {});
      const node = this.searchNodeById(tree, parentId);
      if (node !== undefined) {
        node.nodeChildren = node.nodeChildren.filter(child => child.id !== id);
        this.setState({tree}, this.saveToLocalStorage);
      }
    }
  };

  createNode = (id) => {
    const tree = update(this.state.tree, {});
    const node = this.searchNodeById(tree, id);
    if (node !== undefined) {
      node.nodeChildren.push({name: 'new child', nodeChildren: [], id: Date.now()});
      this.setState({tree}, this.saveToLocalStorage);
    }
  };

  renameNode = (id, value) => {
    const tree = update(this.state.tree, {});
    const node = this.searchNodeById(tree, id);
    if (node !== undefined) {
      node.name = value;
      this.setState({tree}, this.saveToLocalStorage);
    }
  };

  searchNodeById = (node, id) => {
    if (!node) return;
    if (node.id === id) {
      return node;
    } else {
      if (node.nodeChildren.length === 0) return;
      for (let child of node.nodeChildren) {
        const node = this.searchNodeById(child, id);
        if (node !== undefined) return node;
      }
    }
  };

  saveToLocalStorage = () => {
    localStorage.setItem('tree', JSON.stringify(this.state.tree));
  }
}

export default TreeContainer;

import React from "react";
import PropTypes from "prop-types";
import styled from 'styled-components';

const StyledInput = styled.input`
  padding: 5px;
  border: 0;
  background-color: ${props => props.color ? `hsl(${props.color}, 80%, 85%)` : '#fff'};
  margin: 0;
  outline: none;
  transition: box-shadow .2s ease-in-out;
  text-transform: uppercase;
  text-align: center;
  width: 130px;
  &::selection{
    background-color: darkgrey;
  }
  &:hover, &:focus{
    box-shadow: 0 0 10px rgba(0, 0, 0, .7);
  }
`;


class TextField extends React.PureComponent {
  render() {
    const {value, onChange, color} = this.props;
    return (
      <StyledInput value={value} onChange={onChange} title={value} color={color} />
    )
  }
}

TextField.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  color: PropTypes.number
};

export default TextField;

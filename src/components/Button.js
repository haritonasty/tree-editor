import React from "react";
import PropTypes from 'prop-types';
import styled from 'styled-components';


const StyledButton = styled.button`
  margin: 0;
  padding: 8px 10px;
  border: 0;
  border-left: 1px solid #808080;
  color: #808080;
  background-color: #fff;
  outline: none;
  cursor: pointer;
  transition: box-shadow .2s ease-in-out;
  &:hover, &:focus{
    box-shadow: 0 0 10px rgba(0, 0, 0, .7);
  }
`;

class Button extends React.PureComponent {
  render() {
    const {children, title, onClick} = this.props;
    return (
      <StyledButton onClick={onClick} title={title} aria-label={title}>{children}</StyledButton>
    )
  }
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string
};


export default Button;

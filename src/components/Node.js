import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes } from 'styled-components';

import Button from "./Button";
import TextField from "./TextField";

const fadeIn = keyframes`
  from {
    transform: scale(.95);
    opacity: 0;
  }
`;

const NodeContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: baseline;
  margin-left: 20px;
  opacity: ${props => props.out ? '0' : '1'};
  transition: opacity .3s ease-in-out;
`;


const Wrapper = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: flex-start;
  border: 1px solid #808080;
  animation: ${fadeIn} .3s ease-in-out;
`;

class Node extends React.Component {

  state = {out: false};

  render() {
    const {name, nodeChildren, id, parentId, color} = this.props;
    return (
      <NodeContainer out={this.state.out && parentId}>
        <Wrapper>
          <TextField value={name} onChange={this.renameNode} color={color} />
          <Button onClick={this.createNode} title='add child'>✚</Button>
          <Button onClick={this.deleteNode} title='delete node'>✖</Button>
        </Wrapper>
        {nodeChildren.length > 0 && (nodeChildren.map((child) =>
          <Node
            key={child.id}
            id={child.id}
            parentId={id}
            name={child.name}
            nodeChildren={child.nodeChildren}
            color={this.getNextColor(color)}
            deleteNode={this.props.deleteNode}
            createNode={this.props.createNode}
            renameNode={this.props.renameNode}
          />
        ))}
      </NodeContainer>
    )
  }

  createNode = () => {
    this.props.createNode(this.props.id);
  };

  deleteNode = () => {
    this.setState({out: true});
    if (this.props.parentId !== null) {
      setTimeout(() => this.props.deleteNode(this.props.id, this.props.parentId), 300);
    } else {
      this.props.deleteNode(this.props.id, this.props.parentId);
    }
  };

  renameNode = (e) => {
    this.props.renameNode(this.props.id, e.target.value);
  };

  getNextColor = (currColor) => {
    const nextColor = currColor + 20;
    if (nextColor >= 360) return nextColor - 360;
    return nextColor;
  };
}

Node.propTypes = {
  id: PropTypes.number.isRequired,
  parentId: PropTypes.any,
  name: PropTypes.string.isRequired,
  nodeChildren: PropTypes.array.isRequired,
  color: PropTypes.number,
  deleteNode: PropTypes.func.isRequired,
  createNode: PropTypes.func.isRequired,
  renameNode: PropTypes.func.isRequired
};

Node.defaultProps = {
  color: 0,
};

export default Node;

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TreeContainer from "./containers/TreeContainer";

ReactDOM.render(<TreeContainer />, document.getElementById('root'));
